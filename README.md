# Video embed PeerTube

This Drupal module provides a video embed field provider that allows you to
embed videos from any Peertube instance.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/video_embed_peertube).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/video_embed_peertube).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

The requirement is that you already have installed the module video_embed_field.


## Installation

This is like any other module. 

1. Add it to your project with composer
   `"composer require drupal/video_embed_peertube"`.

2. Enable the module(s) and then you can configure it in the settings of your
   field type video_embed_field. In that field you can configure the providers
   and restrict them. Just make sure that if you have restriction you will need
   to adjust the configuration to be able to add remote videos from Peertube.


## Configuration

The configuration is under the field type video_embed_field. It appears in 
the list of allowed providers. If you have restricted the allowed providers 
you will need to specifically add the Peertube provider to the list of allowed 
providers in your field.


## Maintainers

- Philippe Joulot - [phjou](https://www.drupal.org/u/phjou)
